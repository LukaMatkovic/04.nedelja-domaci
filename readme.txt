Zadatak:

Stranicu koju smo uradili za pro�li domaci pro�iriti sa znanjem za ovu nedelju.
domaci 4:
Kreirati web stranicu koja poseduje navigacioni sistem 
primenom listi, formu, slike i tekst. Struktura web sajta da se
odradi primenom div elemenata i float atributa. Stilizovati
elemente forme primenom odgovarajucih CSS selektora i
CSS klasa. Naglasak treba da bude na pravilnoj upotrebi
CSS selektora i CSS klasa kako bi se dobio optimizovani
CSS kod. Obavezno kori�cenje spolja�njeg CSS fajla.

domaci 3:
Koristiti 3 vrste fonta (1 font da bude Arial a druga dva po izboru sa Google Fonts sajta).
Tekst treba da bude ispisan u nekoj proizvoljnoj boji (obavezno preko hexadecimalnih vrednosti).
Za linkove podesiti sva 4 stanja. Linkovi ka omiljenim stranicama treba da budu drugacijeg
stila od primarnih linkova (obavezno koristiti klasu).
Za slike koristiti float atribut. Obavezno koristiti pozadinsku boju i pozadinsku sliku.
Obavezno kori�cenje spolja�njeg CSS fajla.